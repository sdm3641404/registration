function ValidateEmpty(ctrlId,errorControlId, errorMessage)
{
     //Validation For Empty
    var refToControl= document.getElementById(ctrlId);
    var refToErrorControl=document.getElementById(errorControlId);

    if(refToControl.value!="")
    {
        refToErrorControl.innerText = "";
        console.log(refToControl.value);
        localStorage.setItem(ctrlId,refToControl.value);
        return true;
    }
    else
    {
        refToErrorControl.innerText =errorMessage;
        return false;
    }

   
}

function ValidateState()
{
    var refToCmbCountry = document.getElementById("cmbState");
var reftoerrorDivForCountry =
    document.getElementById("errorState");

if (refToCmbCountry.value == '-1') {
    reftoerrorDivForCountry.innerText = "State name is required."
    return false;
}
else {
    reftoerrorDivForCountry.innerText = "";
    console.log(`You selected country with value = ${refToCmbCountry.value}`);
    localStorage.setItem("State",refToCmbCountry.value);
    return true;
}

}
function ValidateChecked()
{
    var ref1 = document.getElementById("Payment_mode1");
    var ref2 = document.getElementById("Payment_mode2");
    var ref3 = document.getElementById("Payment_mode3");
var refToerrorDivForAgr =
    document.getElementById("errorChecked");

if ((ref1.checked && ref2.checked) || (ref3.checked && ref2.checked) || (ref1.checked && ref3.checked)) {
    console.log("Thank you for agreement")
    refToerrorDivForAgr.innerText = "";
    localStorage.setItem("Credit",ref1.checked);
     localStorage.setItem("Debit",ref2.checked);
     localStorage.setItem("UPI",ref3.checked);
    return true;
}
else {
    refToerrorDivForAgr.innerText = "Select atleast 2 checkbox";
    return false;
} 

}
//      function ValidatePin()
//      {
//         var Pincode =document.getElementById("Pin_No");
//         var errTopin =document.getElementById("errorPincode");

//         if(Pincode.value.length != 6){
//             errTopin.innerText="Must be 6 digits"; 
//         }
//         else{
//             errTopin.innerText="";  
//         }
//     }
//    function ValidateMobNo()
//     {
//         var No =document.getElementById("Mob_No");
//         var errToNo =document.getElementById("errorMobile_No");

 
//         if(No.value.length != 10){
//             errToNo.innerText="Must be 10 digits"; 
//         }
//         else{
//             errToNo.innerText="";  
//         }
//     }

function ValidateFixedNo(NumberId,Nolimit,errNoId,errMsg)
{
// debugger
var refNo =document.getElementById(NumberId);
var errDivNo =document.getElementById(errNoId);

if(refNo.value.length != Nolimit){
    errDivNo.innerText= errMsg;
    return false; 
 }
else{
    errDivNo.innerText="";
    localStorage.setItem(NumberId,refNo.value);
    return true;  
 }
 
 
}

function ValidatePassword()
{
var refPass=document.getElementById("Pass_word");
var refNewPass=document.getElementById("newPassword");
var errToPassword=document.getElementById("errorNewPassward");

if(refPass.value==refNewPass.value){
    errToPassword.innerText="Password Matched";
    localStorage.setItem("Pass_word",refPass.value);
    return true;
}else
    {
        errToPassword.innerText="Password is not matching";
        return false;
    }
}